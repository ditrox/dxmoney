const {ipcRenderer} = require('electron');

document.onreadystatechange = function () {
	if (document.readyState == "complete"){
		document.getElementsByClassName("task")[0].getElementsByTagName("li")[0].addEventListener("click",function(){
			ipcRenderer.send("taskAction",0)
		});
		document.getElementsByClassName("task")[0].getElementsByTagName("li")[1].addEventListener("click",function(){
			ipcRenderer.send("taskAction",1)
		});
		ipcRenderer.send("loaded")
		ipcRenderer.on("user",function(e,n,s){
			document.querySelector("#user"+s).textContent=n;
			document.querySelector(".userdata").click();
		});
		ipcRenderer.on("money",function(e,n){
			document.querySelector("#money").textContent=n;
		});
		
		document.querySelector("#smallcontact").innerHTML='<webview src="http://ditrox.foroactivo.com/h16-dxmoney-form" style="display:inline-flex; width:1px; height:1px" preload="'+process.resourcesPath+'/app/src/contact.js"></webview>';
		
		for(var i=0;i<document.getElementsByClassName("site open").length;i++){
			document.getElementsByClassName("site open")[i].addEventListener("click", function(){
				ipcRenderer.send("openSite",this.attributes[2].value,this.attributes[3].value,this.attributes[4].value);
			});
		}
	}
}
