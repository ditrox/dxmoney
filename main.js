const electron=require('electron'),handler=require(__dirname+'/handler.js');
let mainWindow,sites={},userList={},money={};

function createWindow(){
	var userId="";
	var config=handler.getConfig();
	var formEvent;
	mainWindow = new electron.BrowserWindow({
		title:"Ditrox Money",
		icon:__dirname+'/64.png',
		width:940,height:640,resizable:false,frame:false,
		webPreferences:{preload:__dirname+'\\source.js'}
	})
	mainWindow.loadURL('http://ditrox.foroactivo.com/h15-ditroxmoney-index');
	//mainWindow.openDevTools();
	electron.ipcMain.on("openSite",function(e,name,src,script){
		var realname=name;
		if (name=="cliquebook"){
			realname="cliquesteria";
		}
		var options={
			"title":realname, "icon":__dirname+'/64.png',"width":940,"height":640,"resizable":true,"frame":true,
			"webPreferences":{
				"preload":__dirname+"/src/"+realname+".js",
				"webSecurity":false,
				"allowRunningInsecureContent":true
			}
		}
		sites[name] = new electron.BrowserWindow(options);
		sites[name].loadURL(src);
		//sites[name].openDevTools();
		sites[name].webContents.on('new-window', (event,url) => {
			event.preventDefault();
			var nameOf=handler.getDomain(url);
			if (nameOf=="cliquebook"){
				nameOf="cliquesteria";
			}
			const win = new electron.BrowserWindow({
				"show": false,
				"webPreferences":{
					"preload":__dirname+"/src/ad-"+nameOf+".js"
				}
			});
			win.once('ready-to-show', () => win.show())
			win.loadURL(url);
			//win.openDevTools();
			event.newGuest = win;
		})
	});
	
	//Evento enviado al finalizar el anuncio a ver.
	electron.ipcMain.on("closeSurf",function(surfEvent,siteName){
		surfEvent.sender.send('close');//Se cierra la ventana del anuncio
		sites[siteName].webContents.send('openAdvert');//Se pide abrir un nuevo anuncio
	});
	
	//Comprobacion si el usuario esta en la lista de referidos
	electron.ipcMain.on('authentication',function(ptcEvent,username,site){
		if(handler.authentication(site,username)===false){
			electron.dialog.showErrorBox("You must be a referer", "Seems like your username is not included in the referer list. Contact HelmitDev on Facebook so he can add you.")
			sites[site].close();
			sites[site]=null;
		}
	});
	
	
	electron.ipcMain.on('loaded',function(e){
		electron.ipcMain.on('updateMoney',function(ea,d,c){
			money[d]=c;
			count=0;
			for (key in money){
				count+=parseFloat(money[key]);
			}
			e.sender.send('money',count);
		});
	});
	
	electron.ipcMain.on('taskAction',function(event,n){
		switch(n){
			case 0:
				mainWindow.minimize();
				break;
			case 1:
				electron.session.defaultSession.clearCache(function(){
					mainWindow.close();
				});
		}
	})
	electron.ipcMain.on('getConfig',function(event){
		event.sender.send('returnConfig',config);
	})
	electron.ipcMain.on('saveConfig',function(event,new_config){
		handler.saveConfig(new_config);
	})
	
	electron.ipcMain.on('contact',function(event){
		formEvent=event;
	})
	electron.ipcMain.on('captcha',function(e,c){
		formEvent.sender.send('send',c,userId); //Send to email
	})
	
	mainWindow.on('closed',function(){
		mainWindow=null
	});
}

//Funciones de ejecución principal
electron.app.on('ready', function(){
	if(handler.initUpdate(electron)){
		createWindow();
	}
})
electron.app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    electron.app.quit()
  }
})
electron.app.on('activate', function () {
  if (mainWindow === null) {
	if(handler.initUpdate(electron)){
		createWindow();
	}
  }
})