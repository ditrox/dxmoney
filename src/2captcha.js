const {ipcRenderer} = require('electron');
var config={};
ipcRenderer.on("returnConfig",function(e,c){
	config=c;
});
document.onreadystatechange = function () {
	if (document.readyState == "interactive") {
		var iframe=document.createElement("iframe");
		iframe.src='https://2captcha.com/setting';
		iframe.onload=function(){
			config["2captchaApi"]=iframe.contentWindow.document.getElementsByTagName("form")[0].getElementsByTagName("div")[2].getElementsByTagName("td")[0].textContent;
			ipcRenderer.send("saveConfig",config);
		}
		document.body.appendChild(iframe);
		
		ipcRenderer.send("getConfig")
		if(config["storePasswords"]===true && document.URL=="https://2captcha.com/auth/login"){
			if(config["sites"]["2captcha"][0]!==""&&config["sites"]["2captcha"][1]!==""){
				document.querySelector('input[name="email"]').value=config["sites"]["2captcha"][0];
				document.querySelector('input[name="password"]').value=config["sites"]["2captcha"][1];
			} else{
				document.querySelector('input[value="Sign In"]').addEventListener("click", function(){
					config["sites"]["2captcha"]=[document.querySelector('input[name="email"]').value,document.querySelector('input[name="password"]').value];
					ipcRenderer.send("saveConfig",config);
				});
			}
		}
		if(document.querySelector('input[value="Logout"]')&&document.URL.includes("cabinet")){
			ipcRenderer.send("id",document.body.outerHTML.substring(document.body.outerHTML.indexOf("client = '")+"client = '".length,document.body.outerHTML.indexOf(":",document.body.outerHTML.indexOf("client = '"))));
			ipcRenderer.send("updateMoney",document.URL.split('.')[1],document.querySelectorAll(".finbox b")[0].textContent.replace(" $",""));
			var list="";
			setInterval(function(){
				try{
					if(list!=document.querySelector("#mainCaptchaImg").getElementsByTagName("img")[0].src){
						list=document.querySelector("#mainCaptchaImg").getElementsByTagName("img")[0].src;
						ipcRenderer.send("captcha",document.querySelector("#mainCaptchaImg").getElementsByTagName("img")[0].src);
					}
				}catch(e){}
			},1000);
		}
	}
}