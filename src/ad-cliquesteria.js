const {ipcRenderer} = require('electron');
var justOnce=true;
var done=false;
document.onreadystatechange = function () {
	if (justOnce==true && document.readyState == "interactive"){
		var thinker=setInterval(function(){
			if(!document.querySelector("#surfbar").textContent.toLowerCase().includes("wait")&&document.querySelector("#surfbar").style.display=="none"){
				clearInterval(thinker);
				var captchaImage=document.querySelector("#vnumbers").getElementsByTagName("img")[0];
				var can = document.createElement("canvas");
				can.setAttribute("width", captchaImage.naturalWidth);
				can.setAttribute("height", captchaImage.naturalHeight);
				document.body.appendChild(can);
				var ctx = can.getContext("2d");
				ctx.drawImage(captchaImage,0,0);
				var imageData=ctx.getImageData(0,0,can.width,can.height);
				for(var i=0;i<can.width*4;i+=4){
					if(imageData.data[i]+imageData.data[i+1]+imageData.data[i+2]===0){
						var x=captchaImage.getBoundingClientRect().left+parseInt(i/4)+5;
						var y=captchaImage.getBoundingClientRect().top+5;
						for(var i=0;i<=3;i++){
							var ev = new MouseEvent(["mouseover","click","mousedown","mouseout"][i], {
								'view': window,
								'bubbles': true,
								'cancelable': true,
								'screenX': x,
								'screenY': y
							});
							var el = document.elementFromPoint(x, y);
							el.dispatchEvent(ev);
						}
						break;
					}
				}
				var thinker2=setInterval(function(){
					if(document.querySelector("#surfbar").style.display=="block"&&done==false){
						done=true;
						clearInterval(thinker2);
						ipcRenderer.send("closeSurf",document.domain.replace("www.","").split('.')[0])
					}
				},1500);
			} else if(	document.querySelector("#surfbar").textContent.includes("Your click has been validated")||
						document.querySelector("#surfbar").textContent.includes("CONGRATULATIONS!")||
						document.querySelector("#surfbar").textContent.includes("You have already visited")){
				clearInterval(thinker);
				ipcRenderer.send("closeSurf",document.domain.replace("www.","").split('.')[0])
			} else if(document.querySelector("#surfbar").textContent.includes("You have reached your maximum")){
				clearInterval(thinker);
				window.close();
			}
		},1000);
	}
}
ipcRenderer.on("close",function(e){
	window.close();
});