const {ipcRenderer} = require('electron');
var fs = require('fs');
var request = require('request');
var key=null;
var adsclicked={};

function click(element){
	try{
		window.scrollTo(0,element.getBoundingClientRect().top-50);
		for(var i=0;i<=3;i++){
			var ev = new MouseEvent(["mouseover","click","mousedown","mouseout"][i], {
				'view': window,
				'bubbles': true,
				'cancelable': true,
				'screenX': element.getBoundingClientRect().left,
				'screenY': element.getBoundingClientRect().top
			});
			var el = document.elementFromPoint(element.getBoundingClientRect().left,element.getBoundingClientRect().top);
			el.dispatchEvent(ev);
		}
	}catch(e){
		setTimeout(function(){
			click(element);
		},500);
	}
}

function openStuff(){
	try{
		var as=["&","amp",";"];
		var ads=document.getElementsByClassName("ad-block");
		for(var i=0;i<ads.length;i++){
			if(	ads[i].style.visibility!="hidden"&&
			   !ads[i].className.includes("disabled")&&
			   !ads[i].textContent.toLowerCase().includes("do not click")&&
			   !ads[i].getElementsByClassName("ad-footer")[0].textContent.includes("-")&&
				adsclicked[ads[i].querySelector(".pointer").textContent.toLowerCase()]!=true){
					adsclicked[ads[i].querySelector(".pointer").textContent.toLowerCase()]=true;
					click(ads[i].querySelector(".pointer"));
				break;
			}
		}
	}catch(e){
		setTimeout(function(){
			openStuff();
		},500);
	}
}

function autoSolveCaptcha(){
	if(typeof document.getElementById('adcopy-puzzle-image-image') === "object"){
		var frame=document.querySelector("#adcopy-puzzle-image-image");
		var c=document.createElement("canvas");
		c.setAttribute("width",frame.width);
		c.setAttribute("height",frame.height);
		document.body.appendChild(c);
		var ctx=c.getContext("2d");
		ctx.drawImage(frame,0,0);
		var data = c.toDataURL().replace(/^data:image\/\w+;base64,/, "");
		var buf = new Buffer(data, 'base64');
		console.log(__dirname+'\\image.png')
		fs.writeFile(__dirname+'\\image.png', buf,function(){
			var req = request.post('http://imacros2.2captcha.com/getcapcha.php', function (err, resp, body) {
			  if (err) {
				console.log('Error!');
			  } else {
				  document.querySelector("#adcopy_response").value=body;
				  document.querySelector('#validation input[type="submit"]').click();
				  setTimeout(function(){
					  location.reload();
				  },5000);
			  }
			});
			var form = req.form();
			form.append('file', fs.createReadStream(__dirname+'\\image.png'));
			form.append('key', key);
		});			
	}
}

ipcRenderer.on("solved",function(e,c){
	document.getElementById("adcopy_response").value=c;
});


var justOnce=true;
document.onreadystatechange = function () {
	if (justOnce==true && document.readyState == "interactive" && document.URL.includes("/index.php?view=ads") ) {
		ipcRenderer.send("authentication",document.querySelector('li[title="Username"]').textContent.replace(' ',''),document.domain.replace("www.","").split('.')[0]);
		openStuff();
	} else if(justOnce==true && document.readyState == "interactive" && document.location.pathname=="/clixgrid.php") {
		click(document.querySelectorAll(".clixgrid_block table td")[Math.floor(Math.random()*document.querySelectorAll(".clixgrid_block table td").length)])
	
	}
	if ( document.readyState == "complete" && document.URL.includes("/index.php?view=ads") && key !== null) {
		autoSolveCaptcha();
	}
};
var randomList=[];
function openRandomGrid(){
	var randomElement=Math.floor(Math.random()*document.querySelectorAll(".clixgrid_block table td").length);
	if(randomList.indexOf(randomElement)==-1){
		randomList.push(randomElement);
		click(document.querySelectorAll(".clixgrid_block table td")[Math.floor(Math.random()*document.querySelectorAll(".clixgrid_block table td").length)]);
	} else {
		setTimeout(function(){
			openRandomGrid();
		},500);
	}
}
//Ejecuta la funcion para abrir un nuevo anuncio
ipcRenderer.on("openAdvert",function(e){
	if(document.location.pathname=="/clixgrid.php"){
		openRandomGrid();
	} else{
		openStuff();
	}
});